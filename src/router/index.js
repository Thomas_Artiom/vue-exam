import { createRouter, createWebHistory } from 'vue-router'
import TourListView from '../views/TourListView.vue'
import CabView from '../views/CabView.vue'
import TourView from '../views/TourView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: TourListView
  },
  {
    path: '/cabinet',
    name: 'cabinet',
    component: CabView
  },
  {
    path: '/tour/:id',
    name: 'tour',
    component: TourView
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
